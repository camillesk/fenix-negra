class Reconstruction < ApplicationRecord
  require 'matrix'

  # Relationships
  belongs_to :user
  has_one_attached :signal_file
  has_one_attached :img

  # Callbacks
  after_create :set_fields

  def set_fields
    self.rec_begin = DateTime.now
    save
  end

  def file_url
    Rails.application.routes.url_helpers.rails_blob_path(signal_file, disposition: 'attachment', only_path: true)
  end

  def cgne
    File.open('processing/signal.txt', 'w') do |f|
      f.write(signal_file.download.gsub(',', '.'))
    end

    system('python processing/reconstruct.py')

    self.rec_end = DateTime.now
    self.size = 60
    file = File.open(Rails.root.join(Dir[File.join('processing/', '*.*')].max_by(&File.method(:ctime))))
    self.img.attach(io: file, filename: 'reconstruction.png')
    save
  end

  def fista
    self.rec_end = DateTime.now
    save
  end
end
