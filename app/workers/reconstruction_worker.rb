class ReconstructionWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'default'

  def perform(rec_id)
    rec = Reconstruction.find(rec_id)
    if rec.algorithm == 1
      rec.cgne
    else
      rec.fista
    end
  end
end
