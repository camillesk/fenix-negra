json.extract! reconstruction, :id, :algorithm, :rec_begin, :rec_end, :size, :iteractions, :img, :created_at, :updated_at
json.url reconstruction_url(reconstruction, format: :json)
