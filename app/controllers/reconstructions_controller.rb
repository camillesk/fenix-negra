class ReconstructionsController < ApplicationController
  before_action :set_reconstruction, only: [:show, :edit, :update, :destroy]

  # GET /reconstructions
  # GET /reconstructions.json
  def index
    @reconstructions = Reconstruction.where(user_id: current_user.id)
  end

  # GET /reconstructions/1
  # GET /reconstructions/1.json
  def show
  end

  # GET /reconstructions/new
  def new
    @reconstruction = Reconstruction.new
  end

  # GET /reconstructions/1/edit
  def edit
  end

  # POST /reconstructions
  # POST /reconstructions.json
  def create
    params[:reconstruction][:user_id] = current_user.id
    @reconstruction = Reconstruction.new(reconstruction_params)

    respond_to do |format|
      if @reconstruction.save
        ReconstructionWorker.perform_async(@reconstruction.id)
        format.html { redirect_to @reconstruction, notice: 'Reconstrução criada e enfileirada pra processamento' }
        format.json { render :show, status: :created, location: @reconstruction }
      else
        format.html { render :new }
        format.json { render json: @reconstruction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reconstructions/1
  # PATCH/PUT /reconstructions/1.json
  def update
  end

  # DELETE /reconstructions/1
  # DELETE /reconstructions/1.json
  def destroy
    @reconstruction.destroy
    respond_to do |format|
      format.html { redirect_to reconstructions_url, notice: 'Reconstrução excluída' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reconstruction
      @reconstruction = Reconstruction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reconstruction_params
      params.require(:reconstruction).permit(:algorithm, :rec_begin, :rec_end, :size, :iteractions, :signal_file, :user_id)
    end

    def process_signal
      str_signal = signal_file.download
      self.signal = str_signal.split("\r\n")
      save
    end
end
