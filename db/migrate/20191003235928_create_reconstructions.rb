class CreateReconstructions < ActiveRecord::Migration[5.2]
  def change
    create_table :reconstructions do |t|
      t.integer :algorithm
      t.datetime :rec_begin
      t.datetime :rec_end
      t.integer :size
      t.integer :iteractions
      t.binary :img
      t.binary :signal_file
      t.integer :user_id

      t.timestamps
    end
  end
end
