require "application_system_test_case"

class ReconstructionsTest < ApplicationSystemTestCase
  setup do
    @reconstruction = reconstructions(:one)
  end

  test "visiting the index" do
    visit reconstructions_url
    assert_selector "h1", text: "Reconstructions"
  end

  test "creating a Reconstruction" do
    visit reconstructions_url
    click_on "New Reconstruction"

    fill_in "Algorithm", with: @reconstruction.algorithm
    fill_in "Img", with: @reconstruction.img
    fill_in "Iteractions", with: @reconstruction.iteractions
    fill_in "Rec begin", with: @reconstruction.rec_begin
    fill_in "Rec end", with: @reconstruction.rec_end
    fill_in "Size", with: @reconstruction.size
    click_on "Create Reconstruction"

    assert_text "Reconstruction was successfully created"
    click_on "Back"
  end

  test "updating a Reconstruction" do
    visit reconstructions_url
    click_on "Edit", match: :first

    fill_in "Algorithm", with: @reconstruction.algorithm
    fill_in "Img", with: @reconstruction.img
    fill_in "Iteractions", with: @reconstruction.iteractions
    fill_in "Rec begin", with: @reconstruction.rec_begin
    fill_in "Rec end", with: @reconstruction.rec_end
    fill_in "Size", with: @reconstruction.size
    click_on "Update Reconstruction"

    assert_text "Reconstruction was successfully updated"
    click_on "Back"
  end

  test "destroying a Reconstruction" do
    visit reconstructions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Reconstruction was successfully destroyed"
  end
end
