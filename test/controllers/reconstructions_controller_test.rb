require 'test_helper'

class ReconstructionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reconstruction = reconstructions(:one)
  end

  test "should get index" do
    get reconstructions_url
    assert_response :success
  end

  test "should get new" do
    get new_reconstruction_url
    assert_response :success
  end

  test "should create reconstruction" do
    assert_difference('Reconstruction.count') do
      post reconstructions_url, params: { reconstruction: { algorithm: @reconstruction.algorithm, img: @reconstruction.img, iteractions: @reconstruction.iteractions, rec_begin: @reconstruction.rec_begin, rec_end: @reconstruction.rec_end, size: @reconstruction.size } }
    end

    assert_redirected_to reconstruction_url(Reconstruction.last)
  end

  test "should show reconstruction" do
    get reconstruction_url(@reconstruction)
    assert_response :success
  end

  test "should get edit" do
    get edit_reconstruction_url(@reconstruction)
    assert_response :success
  end

  test "should update reconstruction" do
    patch reconstruction_url(@reconstruction), params: { reconstruction: { algorithm: @reconstruction.algorithm, img: @reconstruction.img, iteractions: @reconstruction.iteractions, rec_begin: @reconstruction.rec_begin, rec_end: @reconstruction.rec_end, size: @reconstruction.size } }
    assert_redirected_to reconstruction_url(@reconstruction)
  end

  test "should destroy reconstruction" do
    assert_difference('Reconstruction.count', -1) do
      delete reconstruction_url(@reconstruction)
    end

    assert_redirected_to reconstructions_url
  end
end
