require_relative 'boot'

require 'rails/all'

require 'active_storage/engine'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DISProject
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Set timezone
    config.time_zone = 'America/Sao_Paulo'
    config.active_record.default_timezone = :local

    # Use redis for cache store
    config.cache_store = :redis_cache_store, {url: "#{ENV['REDIS_URL']}/#{ENV['REDIS_CACHE_PATH']}"}

    config.active_job.queue_adapter = :sidekiq
  end
end
