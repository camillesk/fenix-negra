import base64
import datetime
import hashlib
import io
import json
import os
import pickle
import time
import uuid
from json import dumps
from random import randint
from tempfile import TemporaryFile

import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
import scipy.sparse.linalg as sci
from matplotlib import pyplot as plt
from PIL import Image, ImageOps
from scipy import linalg
from scipy.linalg import norm

class ProcessadorImagem():

    # converter txt pra npy
    # model = np.loadtxt(open("model.txt", "rb"), delimiter=",")
    # np.save('model.npy', model)

    signalArray = np.loadtxt(open("processing/signal.txt", "rb"), delimiter="\r\n")

    largura = 60
    altura = 60
    i = 0
    normaRAnterior = 0
    H = np.load('processing/model.npy')
    f = np.zeros((largura*altura), dtype=np.float64)
    Ht = H.transpose()

    r = signalArray - np.dot(H, f)
    p = np.dot(Ht, r)

    while True:
        alpha = np.dot(r.transpose(), r) / np.dot(p.transpose(), p)
        f += np.dot(alpha, p)
        rProximo = r - np.dot(np.dot(alpha, H), p)
        beta = np.dot(rProximo.transpose(), rProximo) / \
            np.dot(r.transpose(), r)
        pProximo = np.dot(Ht, rProximo) + np.dot(beta, p)
        r = rProximo
        p = pProximo

        normaResiduo = abs(np.linalg.norm(r) - normaRAnterior)

        normaRAnterior = np.linalg.norm(r)
        i += 1
        if normaResiduo < 0.0001:
            break

    f.resize(largura, altura)
    nomeImagem = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    plt.imsave('processing/' + nomeImagem + '.png', f, cmap='Greys')
    f_invert = ImageOps.invert(Image.open('processing/' + nomeImagem + '.png').convert('RGB').rotate(-90).transpose(Image.FLIP_LEFT_RIGHT))
    plt.imsave('processing/' + nomeImagem + '_reverted.png', f_invert, cmap='Greys')
